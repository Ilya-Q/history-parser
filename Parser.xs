#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "ppport.h"

#include "const-c.inc"

//history opcodes
enum {
	VFSS_CREAT = 7,
	VFSS_WRITE = 8,
	VFSS_UNLINK = 3,
	VFSS_RENAME = 4,
	VFSS_MKDIR = 5,
	VFSS_RMDIR = 6,
	VFSS_RMDIR_R = 9,
	VFSS_QCTL = 10,
	VFSS_ACL_CTL = 13,
	VFSS_MOUNT_V0 = 14,
	VFSS_MOUNT = 29,
	VFSS_EXPORT = 15,
	VFSS_MKCAT = 32,
	VFSS_ORIGIN = 16,
	VFSS_MKHIER = 11,
	VFSS_RMHIER = 12,
	VFSS_CLEARDIR = 17,
	VFSS_UMOUNT_V0 = 18,
	VFSS_UMOUNT = 30,
	VFSS_UNSHARE = 19,
	VFSS_LOCK = 20,
	VFSS_UNLINK_RC = 21,
	VFSS_RMDIR_RC = 22,
	VFSS_RMDIR_R_RC = 23,
	VFSS_RESTORE_FILE = 24,
	VFSS_RESTORE_DIR = 25,
	VFSS_RESTORE_HIER = 26,
	VFSS_EMPTY_RECYCLE = 27,
	VFSS_CHMOD = 28,
	VFSS_TREE_PROP = 31,
	VFSS_NODEIDUPDATE = 33,
	VFSS_CREAT_UUID = 34,
	VFSS_WRITE_UUID = 35,
	VFSS_MKDIR_UUID = 36,
	VFSS_MKHIER_UUID = 37,
	VFSS_UNLINK_RC_UUID = 38,
	VFSS_RMDIR_RC_UUID = 39,
	VFSS_RMDIR_R_RC_UUID = 40,
	VFSS_RESTORE_FILE_UUID = 41,
	VFSS_RESTORE_DIR_UUID = 42,
	VFSS_RESTORE_HIER_UUID = 43,
};

//mkhier opcodes
enum {
	HIER_DONE = 0,
	HIER_MKNOD = 1,
	HIER_DESCEND = 2,
	HIER_ASCEND = 3,
	HIER_UPDATE = 4,
	HIER_CHDIR = 5,
};

const char* hex_digits = "0123456789ABCDEF";

#define likely(x) __builtin_expect((x),1)

#define unlikely(x) __builtin_expect((x),0)

//another way to skip PUs is to use the read macros, which also gives you error checking
//in theory, the compiler should be clever enough not to compute a value that gets thrown out anyway
//this should still be a bit faster though
#define skip_pu(p) while (*p++ & 0x80)

//the ({}) construct is a GCC extension, so we might want to get rid of those
//it's much perlier this way, though

#define read_pu32(p) ({\
	uint32_t val = (*p & 0x7f);\
	p++;\
	for (uint32_t i = 1; *(p-1) & 0x80; i++, p++) {\
		if (unlikely(i > 5)) croak("pu32 too long");\
		val += (*p & 0x7f) << (i * 7);\
	}\
	val;\
})

#define read_pu64(p) ({\
	uint64_t val = (*p & 0x7f);\
	p++;\
	for (uint32_t i = 1; *(p-1) & 0x80; i++, p++) {\
		if (unlikely(i > 9)) croak("pu64 too long");\
		val += (*p & 0x7f) << (i * 7);\
	}\
	val;\
})


#define skip_string(p) p += read_pu32(p)

#define read_string(p) ({\
	STRLEN len = read_pu32(p);\
	SV* ret = newSV(len);\
	SvCUR_set(ret, len-1);\
	char* t = SvPVX(ret);\
	while (*t++ = *p++);\
	SvPOK_on(ret);\
	ret;\
})

MODULE = History::Parser		PACKAGE = History::Parser		

INCLUDE: const-xs.inc

void extract_versions (SV *input_sv)
	PPCODE:
		char *p = SvPVX(input_sv);
		char *start_p = p;
		//TODO: replace with actual result codes
		if (unlikely(*p++ != 0)) {
			//croak("History doesn't start with CE_SUCCESS");
			ST(0) = sv_2mortal(newSVuv(*(p-1)));
			XSRETURN(1);
		}
		AV *ret = newAV();
		//skip current revision
		skip_pu(p);
		//skip start revision
		skip_pu(p);
		//show number of revisions
		printf("Number of revisions: %ld\n", read_pu64(p));
		//read length of history in bytes
		uint64_t history_len = read_pu64(p);
		uint32_t count = 0;
		while (p < start_p + history_len) {
			char *com_p = p;
			uint32_t com = *p++;
			//skip timestamp
			skip_pu(p);
			//skip userid for commands that include it
			if (com != VFSS_QCTL && com != VFSS_ACL_CTL && com != VFSS_ORIGIN && com != VFSS_TREE_PROP)
				skip_pu(p);
			//skip source id
			skip_pu(p);
			count++;
			switch(com) {
				//these cases can be treated as one since they have the same parameters
				case VFSS_CREAT:
				case VFSS_WRITE: {
					//warn("Reading create or write");
					SV* name = sv_2mortal(read_string(p));
					skip_pu(p);
					SV* mtime = sv_2mortal(newSVuv(read_pu64(p)));
					skip_pu(p);
					SV* hash = sv_2mortal(newSV(40));
					SvCUR_set(hash, 40);
					SvPOK_on(hash);
					char *t = SvPVX(hash);
					for (uint32_t i = 0; i < 20; i++, p++) {
						*t = hex_digits[*p & 0x0f];
						t++;
						*t = hex_digits[(*p >> 4) & 0x0f];
						t++;
					}
					HV* event = newHV();
					hv_stores(event, "date", mtime);
					SvREFCNT_inc(mtime);
					hv_stores(event, "hash", hash);
					SvREFCNT_inc(hash);
					hv_stores(event, "name", name);
					SvREFCNT_inc(name);
					hv_stores(event, "type", newSVpvs("ver"));
					av_push(ret, newRV_noinc((SV*)event));
					break;
				}
				case VFSS_MKHIER: {
					//warn("Reading mkhier");
					//the directory we're currently in
					//MKHIER can traverse the file hierarchy, so we need to keep track of that
					SV* workdir = sv_2mortal(read_string(p));
					//the working directory name doesn't include a trailing /, so we append it
					if (SvCUR(workdir) > 1) sv_catpvs(workdir, "/");
					//skip size
					skip_pu(p);
					uint32_t mkhier_com;
					//it might be a good idea to put this higher so that we don't make a new last_dir for every MKHIER
					SV* last_dir = sv_2mortal(newSV(256)); //255 is the longest a name can be, plus the trailing /
					SvPOK_on(last_dir);
					SvCUR_set(last_dir, 0);
					while ((mkhier_com = *p++) != HIER_DONE) {
						//warn("Workdir is %s", SvPVX(workdir));
						switch(mkhier_com) {
							//these cases are almost equivalent, so we treat them similarly to VFSS_CREAT and VFSS_WRITE
							case HIER_MKNOD:
							case HIER_UPDATE: {
								//warn("About to read an mknod or update");
								//technically, a node could also be VFS_ISHARED or VFS_IMPOINT, but we'll trust the doc here and assume that doesn't happen
								bool is_file = *p++ & 0x01;
								//check if a nodeid is present, if so, skip over it
								if (*p++ & 0x04) p += 16;
								//the length of this node's name
								uint32_t len = *p++;
								if (is_file) {
									//the full name of this file will consist of the working dir + the short name
									//so first we get the buffer with the working directory
									STRLEN workdir_len;
									char *s = SvPV(workdir, workdir_len);
									//make an sv for the filename and get its buffer
									SV* name = sv_2mortal(newSV(workdir_len + len));
									SvCUR_set(name, workdir_len + len);
									SvPOK_on(name);
									char *t = SvPVX(name);
									//copy the working directory
									for (uint32_t i = 0; i < workdir_len; i++)
										*t++ = *s++;
									//copy the file name
									for (uint32_t i = 0; i < len; i++)
										*t++ = *p++;
									*t = '\0';
									//warn("Constructed a name: %s", SvPVX(name));
									SV* mtime = sv_2mortal(newSVuv(read_pu64(p)));
									//skip size
									skip_pu(p);
									SV* hash = sv_2mortal(newSV(40));
									SvCUR_set(hash, 40);
									SvPOK_on(hash);
									t = SvPVX(hash);
									for (uint32_t i = 0; i < 20; i++, p++) {
										*t = hex_digits[*p & 0x0f];
										t++;
										*t = hex_digits[(*p >> 4) & 0x0f];
										t++;
									}
						 			HV* event = newHV();
									hv_stores(event, "date", mtime);
									SvREFCNT_inc(mtime);
									hv_stores(event, "hash", hash);
									SvREFCNT_inc(hash);
									hv_stores(event, "name", name);
									SvREFCNT_inc(name);
									hv_stores(event, "type", newSVpvs("ver"));
									av_push(ret, newRV_noinc((SV*)event));
								} else {
									//we now need to update last_dir in case of a HIER_DESCEND later
									SvCUR_set(last_dir, len + 1);
									//warn("Setting last_dir");
									char *t = SvPVX(last_dir);
									for (uint32_t i = 0; i < len; i++)
										*t++ = *p++;
									*t = '/';
									*(t+1) = '\0';
									//warn("last_dir is now %s", SvPVX(last_dir));
								}
								break;
							}
							case HIER_DESCEND: {
								//warn("About to descend");
								sv_catsv(workdir, last_dir);
								SvCUR_set(last_dir, 0);
								break;
							}
							case HIER_ASCEND: {
								//warn("About to ascend");
								STRLEN newcur;
								char *t = SvPV(workdir, newcur);
								t = t + newcur - 1; //so we end up at the end
								newcur--;
								if (likely(newcur != 0)) {
									while (*--t != '/')
										newcur--;
									SvCUR_set(workdir, newcur);
									*(t+1) = '\0';
								} else {
									warn("Attempt to ascend from / encountered in MKHIER, ignoring it");
								}
								//warn("Workdir is %s after ascend", SvPVX(workdir));
								break;
							}
							case HIER_CHDIR: {
								//warn("About to chdir");
								uint32_t len = *p++;
								STRLEN oldlen = SvCUR(workdir);
								SvGROW(workdir, oldlen + len + 2); //one for the /, one for the trailing 0
								SvCUR_set(workdir, oldlen + len + 1);
								char *t = SvPVX(workdir) + oldlen;
								for (uint32_t i = 0; i < len; i++)
										*t++ = *p++;
								*t = '/';
								*(t+1) = '\0';
								break;
							}
						}
					}
					break;
				}
				case VFSS_RENAME: {
					//warn("Reading rename");
					SV* oldname = sv_2mortal(read_string(p));
					SV* newname = sv_2mortal(read_string(p));
					HV* event = newHV();
					hv_stores(event, "oldname", oldname);
					SvREFCNT_inc(oldname);
					hv_stores(event, "newname", newname);
					SvREFCNT_inc(newname);
					hv_stores(event, "type", newSVpvs("mv"));
					av_push(ret, newRV_noinc((SV*)event));
					break;
				}
				case VFSS_ACL_CTL: {
					//warn("Reading acl_ctl");
					//skip opcode
					p++;
					//skip actor and subject
					skip_pu(p);
					skip_pu(p);
					//skip mask
					p++;
					break;
				}
				case VFSS_QCTL: {
					//warn("Reading qctl");
					//skip opcode
					p++;
					//skip quota
					skip_pu(p);
					break;
				}
				case VFSS_MKDIR: {
					//warn("Reading mkdir");
					//skip dirname
					skip_string(p);
					//skip mode
					skip_pu(p);
					break;
				}
				case VFSS_RMDIR: 
				case VFSS_RMDIR_R:{
					//warn("Reading rmdir");
					skip_string(p);
					break;
				}
				default: {
					croak("Command number %d not implemented at position %d", com, com_p - start_p);
					break;
				}
			}
		}
		printf("%d commands read in total\n", count);
		ST(0) = sv_2mortal(newRV_noinc((SV*)ret));
		XSRETURN(1);
