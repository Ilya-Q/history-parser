use strict;
use warnings;
use ExtUtils::testlib;
use File::Slurp;
use History::Parser;
use Data::Dumper;

my $file = read_file("gethistoryresp-anton-big.bin");
print Dumper(History::Parser::extract_versions($file));